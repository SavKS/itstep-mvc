<?php

use App\View;

app('router')->register(
    'get',
    '/',
    [new \App\Http\Controllers\HomeController, 'index']
);

app('router')->register(
    'get',
    'create/article',
    \App\Http\Controllers\ArticlesController::uses('create'),
    [
        'middleware' => [
            \App\Http\Middleware\Auth::class,
        ],
    ]
);

app('router')->register(
    'post',
    'create/article',
    [new \App\Http\Controllers\ArticlesController, 'store'],
    [
        'middleware' => [
            \App\Http\Middleware\Auth::class,
        ],
    ]
);

app('router')->register(
    'get',
    'articles/view',
    [new \App\Http\Controllers\ArticlesController, 'view']
);

app('router')->register(
    'get',
    'login',
    [new \App\Http\Controllers\AuthController, 'loginForm'],
    [
        'middleware' => [
            \App\Http\Middleware\Guest::class,
        ],
    ]
);

app('router')->register(
    'post',
    'login',
    [new \App\Http\Controllers\AuthController, 'login'],
    [
        'middleware' => [
            \App\Http\Middleware\Guest::class,
        ],
    ]
);

app('router')->register(
    'get',
    'logout',
    [new \App\Http\Controllers\AuthController, 'logout']
);
