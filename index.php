<?php

use App\Application;
use App\Cache\Manager as CacheManager;
use App\Auth\Manager as AuthManager;
use App\Config;
use App\DB;
use App\Interfaces\Responsable;
use App\Request;
use App\Router;
use App\Validation\Validator;
use Intervention\Image\ImageManagerStatic;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFilter;
use Twig\TwigFunction;

error_reporting(E_ALL);

require 'vendor/autoload.php';

require 'handler.php';

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

$app = new Application;

$app->singleton('router', Router::class);

$app->singleton('db', function () {
    $dsn = sprintf(
        'mysql:host=%s;dbname=%s',
        config('database.host'),
        config('database.name')
    );

    $pdo = new PDO(
        $dsn,
        config('database.user'),
        config('database.password'),
        [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ]
    );

    return $pdo;
});

$app->singleton('cache', CacheManager::class);
$app->singleton('config', Config::class);
$app->singleton('auth', AuthManager::class);

$app->singleton('twig', function () {
    $loader = new FilesystemLoader(
        \config('view.path')
    );


    $twig = new Environment($loader);

    $twig->addFilter(
        new TwigFilter('dump', function ($data) {
            dump($data);
        })
    );

    $twig->addFilter(
        new TwigFilter('dd', function ($data) {
            dd($data);
        })
    );

    $twig->addFunction(
        new TwigFunction('old', function ($key = null, $default = null) {
            return Validator::old($key, $default);
        })
    );

    $twig->addFunction(
        new TwigFunction('app', function ($name = null) {
            return app($name);
        })
    );

    $twig->addFunction(
        new TwigFunction('fileUrl', function (array $data, int $width = null, int $height = null) {
            $original = \config('filesystem.base_url') . '/' . $data['hash'] . ($data['ext'] ? ".{$data['ext']}" : '');

            if ($width === null && $height === null) {
                return $original;
            }

            $originalFilePath = \config('filesystem.files_path') . '/' . $data['hash'] . ($data['ext'] ? ".{$data['ext']}" : '');

            $prefix = sprintf(
                '-%s-%s',
                $width ?? 'x',
                $height ?? 'x'
            );

            $pathInfo = pathinfo($originalFilePath);

            $path = sprintf(
                '%s/%s%s.%s',
                $pathInfo['dirname'],
                $pathInfo['filename'],
                $prefix,
                $pathInfo['extension']
            );

            $urlInfo = pathinfo($original);

            $url = sprintf(
                '%s/%s%s.%s',
                $urlInfo['dirname'],
                $urlInfo['filename'],
                $prefix,
                $urlInfo['extension']
            );

            if (! file_exists($path)) {
                $img = ImageManagerStatic::make($originalFilePath);

                $img->resize($width, $height, function ($c) {
                    $c->aspectRatio();
                });

                $img->save($path);
            }

            return $url;
        })
    );

    return $twig;
});

$app->bind(Request::class, Request::class);

require 'routes/web.php';

set_exception_handler(function (Throwable $e) {
    if ($e instanceof Responsable) {
        echo $e->toResponse()->prepareToSend();
    } else {
        throw $e;
    }
});

$app->run(new Request);
