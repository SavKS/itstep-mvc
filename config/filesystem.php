<?php

return [
    'base_path' => __DIR__ . '/../static',
    'files_path' => __DIR__ . '/../static/files',
    'base_url' => '/static/files',
];
