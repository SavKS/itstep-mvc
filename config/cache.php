<?php

use App\Cache\Drivers\FilesystemDriver;

return [
    'default_driver' => env('CACHE_DRIVER'),
    'drivers' => [
        'filesystem' => [
            'processor' => FilesystemDriver::class,
            'config' => [
                'path' => dirname(__DIR__) . '/var/cache'
            ],
        ]
    ],
];
