<?php

return [
    'host' => env('DB_HOST'),
    'user' => env('DB_USER'),
    'password' => env('DB_PASSWORD'),
    'name' => env('DB_NAME'),
];
