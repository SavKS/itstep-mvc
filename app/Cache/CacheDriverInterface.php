<?php

namespace App\Cache;

interface CacheDriverInterface
{
    /**
     * @param string $key
     * @param null|mixed $default
     * @return mixed
     */
    public function get(string $key, $default = null);

    /**
     * @param string $key
     * @param mixed $value
     * @param int $expire
     * @return bool
     */
    public function put(string $key, $value, int $expire): bool;

    /**
     * @param string $key
     * @return bool
     */
    public function forget(string $key): bool;
}
