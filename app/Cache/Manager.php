<?php

namespace App\Cache;

use App\Config;
use Closure;
use RuntimeException;

class Manager
{
    /**
     * @var array
     */
    protected $drivers;

    /**
     * Manager constructor.
     */
    public function __construct()
    {
        $this->drivers = [];
    }

    /**
     * @param string $key
     * @param Closure $resolver
     * @param int $expire
     * @return mixed
     */
    public function remember(string $key, Closure $resolver, int $expire)
    {
        $data = $this->driver()->get($key);

        if ($data === null) {
            $data = $resolver();

            $this->driver()->put($key, $data, $expire);
        }

        return $data;
    }

    /**
     * @param string|null $name
     * @return CacheDriverInterface
     */
    public function driver(string $name = null): CacheDriverInterface
    {
        $name = $name ?? config('cache.default_driver');

        if (! isset($this->drivers[$name])) {
            $this->drivers[$name] = $this->resolveDriver($name);
        }

        return $this->drivers[$name];
    }

    /**
     * @param string $name
     * @return CacheDriverInterface
     */
    protected function resolveDriver(string $name): CacheDriverInterface
    {
        if (! app('config')->has("cache.drivers.{$name}")) {
            throw new RuntimeException("Cache driver [{$name}] not defined");
        }

        $config = config("cache.drivers.{$name}");

        return new $config['processor']($config['config']);
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return $this->driver()->{$name}(...$arguments);
    }
}
