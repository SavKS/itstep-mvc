<?php

namespace App\Cache\Drivers;

use App\Cache\CacheDriverInterface;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class FilesystemDriver implements CacheDriverInterface
{
    /**
     * @var array
     */
    protected $config;

    /**
     * FilesystemDriver constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $key
     * @param null|mixed $default
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        $parts = \explode('.', $key, 2);

        if (\count($parts) > 1) {
            [$key, $query] = $parts;
        } else {
            $key = $key;
            $query = null;
        }

        $pathToFile = $this->pathToFile($key);

        if (! \file_exists($pathToFile)) {
            return $default;
        }

        $content = \file_get_contents(
            $pathToFile
        );

        $expire = Carbon::parse(
            (int)\substr($content, 0, 10)
        );
        $content = \substr($content, 10);

        if (Carbon::now()->greaterThan($expire)) {
            \unlink($pathToFile);

            return $default;
        }

        $data = \unserialize($content);

        return $query !== null ?
            Arr::get( $data, $query, $default) :
            $data;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param int $expire
     * @return bool
     */
    public function put(string $key, $value, int $expire): bool
    {
        $this->checkOrCreateCacheFolder();

        $status = \file_put_contents(
            $this->pathToFile($key),
            Carbon::now()->addMinutes($expire)->getTimestamp() . \serialize($value)
        );

        return $status !== false;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function forget(string $key): bool
    {
        return $this->put($key, null, 0);
    }

    /**
     * @return void
     */
    protected function checkOrCreateCacheFolder(): void
    {
        if (! \is_dir($this->config['path'])
            && ! mkdir($concurrentDirectory = $this->config['path'], 0755, true)
            && ! is_dir($concurrentDirectory)
        ) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }
    }

    /**
     * @param string $key
     * @return string
     */
    protected function pathToFile(string $key): string
    {
        return $this->config['path'] . '/' . \md5($key);
    }
}
