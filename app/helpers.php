<?php

use App\Validation\Validator;

function dd(...$args)
{
    dump(...$args);
    die;
}

function array_get(array $data, string $query, $default = null)
{
    $parts = explode('.', $query);

    $value = $data;

    foreach ($parts as $part) {
        if (is_array($value) && array_key_exists($part, $value)) {
            $value = $value[$part];
        } else {
            $value = null;
        }
    }

    return $value ?? $default;
}

function app(string $name = null)
{
    global $app;

    return $name !== null ? $app->make($name) : $app;
}

function config(string $key = null, $default = null)
{
    if ($key === null) {
        return app('config');
    }

    return app('config')->get($key, $default);
}

/**
 * @return PDO
 */
function db()
{
    return app('db');
}

function uploadFile(array $data)
{
    $path = config('filesystem.files_path');

    $hash = md5_file($data['tmp_name']);

    $fileInfo = pathinfo($data['name']);
    $name = sprintf(
        '%s%s',
        $hash,
        $fileInfo['extension'] ? ".{$fileInfo['extension']}" : ''
    );

    $resultPath = "{$path}/{$name}";

    if (! move_uploaded_file($data['tmp_name'], $resultPath)) {
        throw new RuntimeException('File not saved');
    }

    \db()
        ->prepare('INSERT INTO files (name, hash, size, ext, mime) VALUES (?, ?, ?, ?, ?);')
        ->execute([
            $data['name'],
            $hash,
            filesize($resultPath),
            $fileInfo['extension'],
            mime_content_type($resultPath)
        ]);

    $stmt = \db()->prepare('SELECT * FROM files WHERE id = ?');

    $stmt->execute([
        db()->lastInsertId(),
    ]);

    return $stmt->fetch();
}

function findFile(int $fileId)
{
    $stmt = \db()->prepare('SELECT * FROM files WHERE id = ?');

    $stmt->execute([
        $fileId,
    ]);

    return $stmt->fetch();
}

function findFiles(array $fileIds)
{
    $bindQuery = implode(
        ', ',
        array_fill(0, count($fileIds), '?')
    );

    $stmt = \db()->prepare("SELECT * FROM files WHERE id IN ({$bindQuery})");

    $stmt->execute($fileIds);

    return $stmt->fetchAll();
}
