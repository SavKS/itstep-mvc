<?php

namespace App;

class Response
{
    /**
     * @var mixed
     */
    protected $content;

    /**
     * Response constructor.
     * @param $content
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function prepareToSend(): string
    {
        if (\is_array($this->content)) {
            header('Content-Type: application/json');

            return \json_encode($this->content);
        }

        return (string)$this->content;
    }
}
