<?php

namespace App;

use App\Interfaces\Responsable;

class View implements Responsable
{
    /**
     * @var string
     */
    protected $tpl;

    /**
     * @var array
     */
    protected $data;

    /**
     * View constructor.
     * @param string $tpl
     * @param array $data
     */
    public function __construct(string $tpl, array $data = [])
    {
        $this->tpl = \trim($tpl, '/');
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        return \app('twig')->render($this->tpl, $this->data);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * @return Response
     */
    public function toResponse(): Response
    {
        return new Response(
            $this->render()
        );
    }
}
