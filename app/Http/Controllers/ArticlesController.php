<?php

namespace App\Http\Controllers;

use App\Exceptions\HttpPageNotFound;
use App\Redirect;
use App\Request;
use App\Validation\Validator;
use App\View;
use Illuminate\Support\Str;
use Intervention\Image\Image;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic;

class ArticlesController extends AbstractController
{
    /**
     * @return View
     * @throws HttpPageNotFound
     */
    public function view()
    {
        /** @var Request $request */
        $request = \app(Request::class);

        if (! $request->has('id')) {
            throw new HttpPageNotFound('Page not found');
        }

        $stmt = \db()->prepare('SELECT * FROM articles WHERE id = ? LIMIT 1');

        $stmt->execute([$request->input('id')]);

        $article = $stmt->fetch();

        if (! $article) {
            throw new HttpPageNotFound('Page not found');
        }

        return $this->render('pages/article.twig', [
            'article' => $article,
        ]);
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return $this->render('pages/create-article.twig', [
            'errors' => Validator::restoreErrorsFromSession(),
        ]);
    }

    /**
     * @return Redirect
     */
    public function store(): Redirect
    {
        /** @var Request $request */
        $request = \app(Request::class);

        $validator = Validator::make(
            $request->all(),
            [
                'title' => [
                    'required',
                    'string',
                    'max:255',
                ],
                'slug' => [
                    'nullable',
                    'string',
                    'max:255',
                    'regexp:/^[\w-_]+$/',
                ],
                'body' => [
                    'required',
                    'string',
                    'max:5000',
                ],
            ],
            [
                'required' => 'Введіть значення',
                'string' => 'Значення повинно бути текстом',
                'max' => 'Значення не повинно бути більше %s символів',
                'regexp' => 'Значення не відповідає шаблону',
            ]
        );

        $validator->validate();

        if ($validator->hasErrors()) {
            $validator->saveToSession();

            return Redirect::make('/create/article');
        }

        $file = \uploadFile($_FILES['preview']);

        \db()
            ->prepare('INSERT INTO articles (title, user_id, file_id, slug, body) VALUES (?, ?, ?, ?, ?);')
            ->execute([
                $request->input('title'),
                \app('auth')->user()['id'],
                $file['id'],
                $request->input('slug') ?: Str::slug(
                    $request->input('title')
                ),
                $request->input('body'),
            ]);

        return Redirect::make('/');
    }
}
