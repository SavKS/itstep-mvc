<?php

namespace App\Http\Controllers;

use App\Http\Test;
use App\Request;
use Illuminate\Support\Arr;
use PDO;

class HomeController extends AbstractController
{
    /**
     * @return \App\View
     */
    public function index()
    {
        /** @var Request $request */
        $request = \app(Request::class);

        $currentPage = $request->input('page', 1);

        if (! \is_numeric($currentPage)) {
            $currentPage = 1;
        } else {
            $currentPage = max(1, $currentPage);
        }

        $perPage = 25;

        $offset = ($currentPage - 1) * $perPage;

        $q = $request->input('q');
        $term = \is_string($q) && ! empty($q) ? $q : null;
        $st = db()->prepare(
            \sprintf(
                'SELECT articles.*, users.name as author FROM articles INNER JOIN users ON users.id = articles.user_id %s LIMIT :offset, :per_page %s',
                'ORDER BY created_at DESC',
                $term ? 'WHERE title LIKE :term' : ''
            )
        );

        $st->bindValue('offset', $offset, PDO::PARAM_INT);
        $st->bindValue('per_page', $perPage + 1, PDO::PARAM_INT);

        if ($term) {
            $st->bindValue('term', "%{$term}%");
        }

        $st->execute();

        $articles = $st->fetchAll();

        return $this->render('pages/home.twig', [
            'articles' => $articles,
            'currentPage' => $currentPage,
            'previews' => \collect(
                \findFiles(
                    Arr::pluck($articles, 'file_id')
                )
            )->keyBy('id')->all()
        ]);
    }
}
