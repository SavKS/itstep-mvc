<?php

namespace App\Http\Controllers;

use App\Redirect;
use App\Request;
use App\Validation\Validator;
use App\View;

class AuthController extends AbstractController
{
    /**
     * @return View
     */
    public function loginForm(): View
    {
        return $this->render('pages/login.twig', [
            'errors' => Validator::restoreErrorsFromSession(),
        ]);
    }

    /**
     * @return Redirect
     */
    public function login(): Redirect
    {
        $request = \app(Request::class);

        $validator = Validator::make(
            $request->all(),
            [
                'login' => [
                    'required',
                    'string',
                ],
                'password' => [
                    'required',
                    'string',
                ],
            ]
        );

        $validator->validate();

        if ($validator->hasErrors()) {
            $validator->saveToSession();

            return Redirect::make('/login');
        }

        $user = \app('auth')->login(
            $request->input('login'),
            $request->input('password')
        );

        if (! $user) {
            return Redirect::make('/login')->withErrors([
                'items' => [
                    'credentials' => [
                        'rule' => 'not_match',
                    ],
                ],
            ]);
        }
        
        return Redirect::make('/');
    }

    /**
     * @return Redirect
     */
    public function logout(): Redirect
    {
        app('auth')->logout();
        
        return Redirect::make('/');
    }
}
