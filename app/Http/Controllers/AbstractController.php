<?php

namespace App\Http\Controllers;

use App\Http\CanViewTrait;

abstract class AbstractController
{
    use CanViewTrait;

    /**
     * @param string $method
     * @return array
     */
    public static function uses(string $method): array
    {
        return [static::class, $method];
    }
}
