<?php

namespace App\Http;

use App\View;

trait CanViewTrait
{
    /**
     * @param string $tpl
     * @param array $data
     * @return View
     */
    public function render(string $tpl, array $data = []): View
    {
        return new View($tpl, $data);
    }
}
