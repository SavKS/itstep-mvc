<?php

namespace App\Http\Middleware;

use App\Request;
use Closure;

class StartSession
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (! \session_id()) {
            \session_start();
        }

        return $next($request);
    }
}
