<?php

namespace App\Http\Middleware;

use App\Redirect;
use App\Request;
use Closure;

class Auth
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (! app('auth')->check()) {
            return Redirect::make('/login');
        }

        return $next($request);
    }
}
