<?php

namespace App\Exceptions;

use App\Interfaces\Responsable;
use App\Response;
use App\View;

class HttpMethodNotAllowed extends \Exception implements Responsable
{
    //
    /**
     * @return Response
     */
    public function toResponse(): Response
    {
        return (new View('errors/405.twig'))->toResponse();
    }
}
