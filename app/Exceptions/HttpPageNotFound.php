<?php

namespace App\Exceptions;

use App\Interfaces\Responsable;
use App\Response;
use App\View;

class HttpPageNotFound extends \Exception implements Responsable
{
    //
    /**
     * @return Response
     */
    public function toResponse(): Response
    {
        return (new View('errors/404.twig'))->toResponse();
    }
}
