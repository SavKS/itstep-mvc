<?php

namespace App;

class Redirect
{
    /**
     * @var string
     */
    public $to;

    /**
     * @var int
     */
    public $statusCode;

    /**
     * Redirect constructor.
     * @param string $to
     * @param int $statusCode
     */
    public function __construct(string $to, int $statusCode = 302)
    {
        $this->to = $to;
        $this->statusCode = $statusCode;
    }

    /**
     * @param array $errors
     * @return $this
     */
    public function withErrors(array $errors): self
    {
        $_SESSION['errors'] = $errors;

        return $this;
    }

    /**
     * @param string $to
     * @param int $statusCode
     * @return static
     */
    public static function make(string $to, int $statusCode = 302): Redirect
    {
        return new static($to, $statusCode);
    }
}
