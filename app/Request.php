<?php

namespace App;

use Illuminate\Support\Arr;

class Request
{
    /**
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function input(string $key, $default = null)
    {
        return $_REQUEST[$key] ?? $default;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return ! empty($_REQUEST[$key]);
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $_REQUEST;
    }

    /**
     * @return string
     */
    public function method(): string
    {
        return \strtolower($_SERVER['REQUEST_METHOD']);
    }

    /**
     * @return string
     */
    public function uri(): string
    {
        $parts = \explode('?', $_SERVER['REQUEST_URI']);

        $uri = \mb_strtolower(
            \trim($parts[0], '/')
        );

        return "/{$uri}";
    }
}
