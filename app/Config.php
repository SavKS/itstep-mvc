<?php

namespace App;

use Illuminate\Support\Arr;

/**
 * Class Config
 * @package App
 */
class Config
{
    /**
     * @var array
     */
    protected $data;

    /**
     * Config constructor.
     */
    public function __construct()
    {
        $this->data = [];

        $configDir = \dirname(__DIR__) . '/config';

        foreach (glob("{$configDir}/*.php") as $file) {
            $name = \basename($file, '.php');

            $this->data[$name] = require $file;
        }
    }

    /**
     * @param string $key
     * @param null $default
     * @return array|mixed|null
     */
    public function get(string $key, $default = null)
    {
        return $this->query($key, $default);
    }

    /**
     * @param string $key
     * @param null $default
     * @return array|mixed|null
     */
    public function query(string $key, $default = null)
    {
        return Arr::get($this->data, $key, $default);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return Arr::has($this->data, $key);
    }
}
