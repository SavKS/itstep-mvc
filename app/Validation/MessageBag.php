<?php

namespace App\Validation;

class MessageBag
{
    /**
     * @var array
     */
    public $items;

    /**
     * @var array
     */
    public $messages;

    /**
     * MessageBag constructor.
     * @param array $items
     * @param array $messages
     */
    public function __construct(array $items, array $messages = [])
    {
        $this->items = $items;
        $this->messages = $messages;
    }

    /**
     * @param string $field
     * @return bool
     */
    public function has(string $field): bool
    {
        return isset($this->items[$field]);
    }

    /**
     * @param string $field
     * @return string|null
     */
    public function get(string $field)
    {
        if (! $this->has($field)) {
            return null;
        }

        $rule = $this->items[$field]['rule'] ?? '';

        if (isset($this->messages["{$field}.{$rule}"])) {
            $message = $this->messages["{$field}.{$rule}"];
        } elseif (isset($this->messages[$rule])) {
            $message = $this->messages[$rule];
        } else {
            $message = $rule;
        }

        return ! empty($this->items[$field]['arguments']) ?
            \sprintf($message, ...$this->items[$field]['arguments']) :
            $message;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        $result = [];

        foreach ($this->items as $field => $item) {
            $result[$field] = $this->get($field);
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return empty($this->items);
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return $this->messages;
    }
}
