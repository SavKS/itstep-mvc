<?php

namespace App\Validation;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class Validator
{
    /**
     * @var MessageBag
     */
    protected $errors;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var array
     */
    protected $rules;

    /**
     * @var array
     */
    protected $messages;

    /**
     * @var array|null
     */
    protected static $oldInputs;

    /**
     * Validator constructor.
     * @param array $data
     * @param array $rules
     * @param array $messages
     */
    public function __construct(array $data, array $rules, array $messages = [])
    {
        $this->data = $data;
        $this->rules = $rules;
        $this->messages = $messages;
    }

    /**
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @return static
     */
    public static function make(array $data, array $rules, array $messages = []): Validator
    {
        return new static($data, $rules, $messages);
    }

    /**
     * @return $this
     */
    public function validate(): self
    {
        $errors = [];

        foreach ($this->rules as $field => $rules) {
            $firstRule = \array_shift($rules);

            if ($firstRule === 'nullable' && $this->isEmpty($this->data[$field] ?? '')) {
                continue;
            }

            if ($firstRule === 'required' && $this->isEmpty($this->data[$field] ?? '')) {
                $errors[$field] = [
                    'rule' => 'required',
                    'arguments' => [],
                ];

                continue;
            }

            foreach ($rules as $rule) {
                $ruleParts = \explode(':', $rule);

                $method = Str::studly($ruleParts[0]);

                $arguments = isset($ruleParts[1]) ? explode(',', $ruleParts[1]) : [];

                if (! $this->{'validate' . $method}($this->data[$field], $arguments)) {
                    $errors[$field] = [
                        'rule' => $ruleParts[0],
                        'arguments' => $arguments,
                    ];

                    break;
                }
            }
        }

        $this->errors = new MessageBag($errors, $this->messages);

        return $this;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function isEmpty($value): bool
    {
        if (\is_array($value)) {
            return empty($value);
        }

        return empty(\trim($value));
    }

    /**
     * @param $value
     * @return bool
     */
    protected function validateString($value): bool
    {
        return \is_string($value);
    }

    /**
     * @param $value
     * @param int $min
     * @return bool
     */
    protected function validateMin($value, int $min): bool
    {
        return \mb_strlen($value) >= $min;
    }

    /**
     * @param $value
     * @param array $args
     * @return bool
     */
    protected function validateMax($value, array $args): bool
    {
        return \mb_strlen($value) <= $args[0];
    }

    /**
     * @param $value
     * @param array $args
     * @return bool
     */
    protected function validateRegexp($value, array $args): bool
    {
        return (bool)\preg_match($args[0], $value);
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return $this->errors && ! $this->errors->isEmpty();
    }

    /**
     * @return MessageBag
     */
    public static function restoreErrorsFromSession(): MessageBag
    {
        $validator = new MessageBag(
            $_SESSION['errors']['items'] ?? [],
            $_SESSION['errors']['messages'] ?? []
        );

        $_SESSION['errors'] = [];

        return $validator;
    }

    /**
     * @return void
     */
    public function saveToSession(): void
    {
        if ($this->errors) {
            $_SESSION['errors'] = [
                'items' => $this->errors->items,
                'messages' => $this->errors->messages,
            ];
        }

        if ($this->data) {
            $_SESSION['_old_data'] = $this->data;
        }
    }

    /**
     * @return array|null
     */
    protected static function getOldInputs(): ?array
    {
        if (static::$oldInputs === null) {
            static::$oldInputs = $_SESSION['_old_data'] ?? [];

            $_SESSION['_old_data'] = [];
        }

        return static::$oldInputs;
    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public static function old(string $key = null, $default = null)
    {
        if ($key === null) {
            return static::getOldInputs();
        }

        return Arr::get(
            static::getOldInputs(),
            $key,
            $default
        );
    }
}
