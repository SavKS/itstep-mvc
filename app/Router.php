<?php

namespace App;

use App\Exceptions\HttpMethodNotAllowed;
use App\Exceptions\HttpPageNotFound;
use App\Interfaces\Responsable;
use Closure;
use LogicException;

class Router
{
    /**
     * @var array
     */
    protected $routes;

    /**
     * @var array
     */
    protected static $allowedMethods = ['get', 'post'];

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->routes = [];
    }

    /**
     * @param string $method
     * @param string $uri
     * @param callable $handler
     * @param array $config
     */
    public function register(string $method, string $uri, $handler, array $config = []): void
    {
        $normalizedUri = \mb_strtolower(
            \trim($uri, '/')
        );

        $normalizedMethod = \strtolower($method);

        if (! \in_array($normalizedMethod, static::$allowedMethods, true)) {
            throw new \InvalidArgumentException("Method [{$method}] not allowed");
        }

        $this->routes["/{$normalizedUri}"][$normalizedMethod] = [
            'handler' => $handler,
            'middleware' => $config['middleware'] ?? [],
        ];
    }

    /**
     * @return array
     */
    public function routes(): array
    {
        return $this->routes;
    }

    /**
     * @param Request $request
     * @throws HttpPageNotFound
     * @throws HttpMethodNotAllowed
     */
    public function dispatch(Request $request): void
    {
        if (! isset($this->routes[$request->uri()])) {
            throw new HttpPageNotFound("Route not found [{$request->uri()}]");
        }

        if (! isset($this->routes[$request->uri()][$request->method()])) {
            throw new HttpMethodNotAllowed('Route method not allowed');
        }

        $middlewareClasses = \array_merge(
            config('http.middleware', []),
            $this->routes[$request->uri()][$request->method()]['middleware'] ?? []
        );

        \ob_start();

        /** @var Response $response */
        $response = $this->runMiddlewarePipeline(
            $request,
            $middlewareClasses,
            function (Request $request) {
                $content = \call_user_func($this->routes[$request->uri()][$request->method()]['handler']);

                if ($content instanceof Responsable) {
                    $response = $content->toResponse();
                } elseif ($content instanceof Redirect) {
                    return $content;
                } elseif (\is_string($content) || \is_array($content) || $content === null) {
                    $response = new Response($content);
                } else {
                    throw new LogicException('Content is not responsable');
                }

                return $response;
            }
        );

        $content = \ob_get_contents();

        \ob_clean();

        if ($response instanceof Redirect) {
            \header('Location:' . $response->to);

            http_response_code($response->statusCode);
        } else {
            $content .= $response ? $response->prepareToSend() : '';

            echo $content;
        }
    }

    /**
     * @param Request $request
     * @param array $middlewareClasses
     * @param Closure $handler
     * @return mixed
     */
    protected function runMiddlewarePipeline(Request $request, array $middlewareClasses, Closure $handler)
    {
        $current = \array_shift($middlewareClasses);

        if (! $current) {
            return $handler($request);
        }

        return (new $current)->handle($request, function (Request $request) use ($handler, $middlewareClasses) {
            return $this->runMiddlewarePipeline($request, $middlewareClasses, $handler);
        });
    }
}
