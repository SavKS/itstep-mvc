<?php

namespace App\Interfaces;

use App\Response;

interface Responsable
{
    /**
     * @return Response
     */
    public function toResponse(): Response;
}
