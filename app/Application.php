<?php

namespace App;

use Closure;
use RuntimeException;

class Application
{
    /**
     * @var array
     */
    protected $items;

    /**
     * @var array
     */
    protected $instances;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var DB
     */
    protected $db;

    /**
     * Application constructor.
     */
    public function __construct()
    {
        $this->items = [];
        $this->instances = [];
    }

    /**
     * @param string $name
     * @param Closure|string $handler
     */
    public function singleton(string $name, $handler)
    {
        $this->items[$name] = [
            'handler' => $handler,
            'share' => true,
        ];
    }

    /**
     * @param string $name
     * @param Closure|string $handler
     */
    public function bind(string $name, $handler)
    {
        $this->items[$name] = [
            'handler' => $handler,
            'share' => false,
        ];
    }

    /**
     * @param Request $request
     */
    public function run(Request $request)
    {
        $this->make('router')->dispatch($request);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function make(string $name)
    {
        if (! \array_key_exists($name, $this->items)) {
            throw new RuntimeException("[{$name}] not found");
        }

        $handler = $this->items[$name]['handler'];

        if (! $this->items[$name]['share']) {
            return \is_string($handler) ? new $handler : \call_user_func($handler);
        }

        if (! \array_key_exists($name, $this->instances)) {
            $this->instances[$name] = \is_string($handler) ? new $handler : \call_user_func($handler);
        }

        return $this->instances[$name];
    }
}
