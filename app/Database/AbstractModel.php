<?php

namespace App\Database;

use Illuminate\Support\Str;
use PDO;

abstract class AbstractModel
{
    /**
     * @var array
     */
    protected $guard = [];

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * AbstractModel constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
    }

    /**
     * @return string
     */
    public function getTable(): string
    {
        $className = \class_basename(static::class);

        return Str::lower(
            Str::plural($className)
        );
    }

    /**
     * @return string
     */
    public function getKeyName(): string
    {
        return 'id';
    }

    /**
     * @return mixed|null
     */
    public function getKey()
    {
        return $this->attributes[$this->getKeyName()] ?? null;
    }

    /**
     * @param mixed $id
     * @return static|null
     */
    public function find($id)
    {
        /** @var PDO $pdo */
        $pdo = \app('db');

        $st = $pdo->prepare("SELECT * FROM {$this->getTable()} WHERE {$this->getKeyName()} = ? LIMIT 1");

        $st->execute([$id]);

        $record = $st->fetch();

        if (! $record) {
            return null;
        }

        $result = [];

        foreach ($record as $name => $value) {
            $result[$name] = $value;
        }

        return new static($result);
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return $this->attributes[$name] ?? null;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        if (! \in_array($name, $this->guard, true)) {
            $this->attributes[$name] = $value;
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
        return \array_key_exists($name, $this->attributes);
    }

    public function save()
    {
        /** @var PDO $pdo */
        $pdo = \app('db');

        $fields = [];
        $values = [];

        foreach ($this->attributes as $name => $value) {
            if (\in_array($name, ['id', 'created_at'], true)) {
                continue;
            }

            $fields[] = "{$name} = ?";
            $values[] = $value;
        }

        $fieldsQuery = \implode(', ', $fields);

        $st = $pdo->prepare("UPDATE {$this->getTable()} SET {$fieldsQuery} WHERE {$this->getKeyName()} = ?;");

        $st->execute(
            \array_merge($values, [$this->getKey()])
        );
    }
}
