<?php

namespace App\Auth;

class Manager
{
    /**
     * @var array
     */
    protected $user;

    /**
     * @param string $login
     * @param string $password
     * @return bool
     */
    public function login(string $login, string $password): bool
    {
        $stmt = \db()->prepare('SELECT * FROM users WHERE login = ?');

        $stmt->execute([$login]);

        $user = $stmt->fetch();

        if (! $user) {
            return false;
        }

        if (! \password_verify($password, $user['password'])) {
            return false;
        }

        $_SESSION['__auth_user'] = $user['id'];

        $this->user = $user;

        return true;
    }

    /**
     * @return void
     */
    public function logout(): void
    {
        $_SESSION['__auth_user'] = null;

        $this->user = null;
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        return (bool)$this->user();
    }

    /**
     * @return array|null
     */
    public function user(): ?array
    {
        if ($this->user === null) {
            if (! $userId = $_SESSION['__auth_user'] ?? null) {
                return null;
            }

            $stmt = \db()->prepare('SELECT * FROM users WHERE id = ?');

            $stmt->execute([$userId]);

            if (! $user = $stmt->fetch()) {
                return null;
            }

            $this->user = $user;
        }

        return $this->user;
    }
}
